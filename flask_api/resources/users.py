import json

from flask import jsonify, Blueprint, abort, make_response

from flask_restful import (Resource, Api, reqparse,
                            inputs, fields, marshal,
                            marshal_with, url_for)

import models

user_fields = {
    'username': fields.String
}


def get_user_or_404(id):
    try:
        user = models.User.get(models.User.id==id)
    except models.User.DoesNotExist:
        abort(404)
    else:
        return user


class UserList(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            'username',
            required=True,
            help='No username provided.',
            location=['form', 'json']
        )
        self.reqparse.add_argument(
            'email',
            required=True,
            help='No email provided.',
            location=['form', 'json']
        )
        self.reqparse.add_argument(
            'password',
            required=True,
            help='No password provided.',
            location=['form', 'json']
        )
        self.reqparse.add_argument(
            'verify_password',
            required=True,
            help='No password verification provided.',
            location=['form', 'json']
        )
        super().__init__()

    def get(self):
        return [marshal(user, user_fields) for user in models.User.select()]

    #@marshal_with(user_fields)
    def post(self):
        args = self.reqparse.parse_args()
        if args.get('password') == args.get('verify_password'):
            user = models.User.create_user(**args)
            return (user, user_fields), 201
        return make_response(
            json.dumps({
                    'error': 'Password and password verification do not match.'
                }), 400)


class User(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            'username',
            required=True,
            location=['form', 'json']
        )
        self.reqparse.add_argument(
            'email',
            required=True,
            location=['form', 'json']
        )
        self.reqparse.add_argument(
            'password',
            required=True,
            location=['form', 'json']
        )
        super().__init__()

    @marshal_with(user_fields)
    def get(self, id):
        return get_user_or_404(id)

    @marshal_with(user_fields)
    def put(self, id):
        user = get_user_or_404(id)
        args = self.reqparse.parse_args()
        query = models.User.update(**args).where(models.User.id==id)
        query.execute()
        return (get_user_or_404(id), 200, {
            'Location': url_for('resources.users.user', id=user.id)
        })

    def delete(self, id):
        user = get_user_or_404(id)
        query = models.User.delete().where(models.User.id==id)
        query.execute()
        return '', 204, {'Location': url_for('resources.users.users')}


users_api = Blueprint('resources.users', __name__)
api = Api(users_api)
api.add_resource(
    UserList,
    '/users',
    endpoint='users'
)
