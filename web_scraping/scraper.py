from urllib.request import urlopen
from bs4 import BeautifulSoup

html = urlopen('https://treehouse-projects.github.io/horse-land/index.html')
soup = BeautifulSoup(html.read(), 'html.parser')

# Prints the page scraped and indent it properly.
#print(soup.prettify())

# Prints the <title> of the page.
#print(soup.title)

# Print the first <div> of the page.
#print(soup.div)

# Prints all the <div> tags of the page.
# divs = soup.find_all('div')
# for div in divs:
#     print(div)

# Prints all the <div> containing a class="featured".
# divs = soup.find_all('div', {'class':'featured'})
# for div in divs:
#     print(div)

# Find the first <div> containing a class="featured".
# divs = soup.find('div', {'class':'featured'})
# print(divs)

# Prints the first <h2> within a <div> containing a class="featured".
# featured_header = soup.find('div',  {'class':'featured'}).h2
# Get text will strip all <tags> from the source.
# Make sure to use it in the last steps of scraping so your source keeps its <tags>.
# print(featured_header.get_text())

# for button in soup.find(attrs={'class':'button button--primary'}):
#     print(button)

# This is equivalent to this:
# for button in soup.find(class_='button button--primary'):
#     print(button)

# Get all the links on the page.
for link in soup.find_all('a'):
    print(link.get('href'))
