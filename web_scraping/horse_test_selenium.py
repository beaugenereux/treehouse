from bs4 import BeautifulSoup
from selenium import webdriver

import time

# Starts the driver to automate data scraping.
driver = webdriver.Chrome()

# Go to this URL.
driver.get('https://treehouse-projects.github.io/horse-land/index.html')

# Wait for everything to load.
time.sleep(5)

# Pulls the page source.
page_html = driver.page_source

# Parses the page source and intend it nicely.
soup = BeautifulSoup(page_html, 'html.parser')

print(soup.prettify())

# Stop the driver and close the automated website.
driver.close()
