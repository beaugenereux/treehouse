from urllib.request import urlopen
from bs4 import BeautifulSoup

import re

# CHALLENGE: Fetch all external urls.

# An empty array to store the links.
links_int = []
links_ext = []

# Fetches all links.
def all_links(linkURL):
    #print('## inside def all_links()')
    html = urlopen('https://treehouse-projects.github.io/horse-land/'.format(linkURL))
    soup = BeautifulSoup(html, 'html.parser')
    # Get all the links on the page.
    for link in soup.find_all('a'):
        while len(links_int) > 0:
            page = link.attrs['href']
            if page not in links_int and page not in links_ext:
                links_ext.append(page)
                #print(page)
            else:
                break

# Fetches internal links only.
def internal_links(linkURL):
    html = urlopen('https://treehouse-projects.github.io/horse-land/{}'.format(linkURL))
    soup = BeautifulSoup(html, 'html.parser')

    return soup.find('a', href=re.compile('(.html)$'))


# I read about this but don't understand it yet.
if __name__ == '__main__':
    #print('## inside if __name__')
    urls = internal_links('index.html')
    while len(urls) > 0:
        page = urls.attrs['href']
        if page not in links_int:
            links_int.append(page)
            #print(page)
            #print('\n')
            urls = internal_links(page)
        else:
            all_links(page)
            break

print('links_int =', links_int)
print('links_ext =', links_ext)
