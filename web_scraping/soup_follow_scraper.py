from urllib.request import urlopen
from bs4 import BeautifulSoup

import re

# Stores the links so we don't loop on the site.
site_links = []

def internal_links(linkURL):
    html = urlopen('https://treehouse-projects.github.io/horse-land/{}'.format(linkURL))
    soup = BeautifulSoup(html, 'html.parser')

    return soup.find('a', href=re.compile('(.html)$'))


# While on the main dir, prints all internal links.
if __name__ == '__main__':
    urls = internal_links('index.html')
    while len(urls) > 0:
        page = urls.attrs['href']
        if page not in site_links:
            site_links.append(page)
            print(page)
            print('\n')
            urls = internal_links(page)
        else:
            break
