from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

class HorseSpider(CrawlSpider):

    name = 'earl'

    allowed_domains = ['treehouse-projects.github.io']

    start_urls = ['http://treehouse-projects.github.io/horse-land']

    rules = [Rule(LinkExtractor(allow=r'.*'),
                  callback='parse_horses',
                  follow=True)]

    def parse_horses(self, response):
        url = response.url
        # Fetches the entire <title> tag.
        #title = response.css('title')
        # Fetches the entire <title> tag and extracts and returns its text only.
        title = response.css('title::text').extract()
        print('Page URL: {}.\nPage title: {}'.format(url, title))
