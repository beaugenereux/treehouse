from scrapy.http import FormRequest
from scrapy.spiders import Spider


class FormSpider(Spider):

    name = 'horseForm'

    start_urls = ['https://treehouse-projects.github.io/horse-land/form.html']

    def parse(self, response):
        formdata =  {'firstname': 'Ken',
                     'lastname': 'Alger',
                     'jobtitle': 'Teacher'}

        # Submits the formdata above to the form and submits it.
        return FormRequest.from_response(response, formnumber=0,
                                         formdata=formdata,
                                         callback=self.after_post)


    def after_post(self, response):
        #print('Form processed.\n', response)
        print('\n\n****** Form processed ******\n\n')
        # Prints <200 https://formspree.io/content+scrapy@teamtreehouse.com> instead of <200 https://formspree.io/ken.alger+scrapy@teamtreehouse.com>
        print(response)
        # Prints the entire page html, js and all
        #print(response.text)
        print('\n\n****** Script completed ******\n\n')
