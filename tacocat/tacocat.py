#!/Users/renard/code/treehouse/tacocat/venv/bin/python3

from flask import (Flask, g, render_template, flash, redirect, url_for,
                    abort)
from flask_bcrypt import check_password_hash
from flask_login import (LoginManager, login_user, logout_user,
                            login_required, current_user)

import forms
import models


DEBUG = True
PORT = 8080
HOST = '0.0.0.0'

app = Flask(__name__)
app.secret_key = '<skey>ua~a&n;fê!4%k9lgn*--0000</skey>'

login_manager = LoginManager()
login_manager.init_app(app)
### If user not login, redirect to login... for now.
login_manager.login_view = 'login'


@login_manager.user_loader
def load_user(user_id):
    try:
        return models.User.get(models.User.id == user_id)
    except models.DoesNotExist:
        return None


@app.before_request
def before_request():
    """Connect to the database before each request."""
    g.db = models.DATABASE
    ##### BANDAID SOLUTION #####
    # https://teamtreehouse.com/community/tests-failing-and-i-cant-figure-out-why-getting-peeweeoperationalerror-connection-already-open-please-help
    #g.db.close()
    g.db.connect()
    # Find current user, flask_login methods.
    g.user = current_user


@app.after_request
def after_request(response):
    """Close the database connection after each request."""
    g.db.close()
    return response


### USER SIGN UP
@app.route('/register', methods=('GET', 'POST'))
def register():
    form = forms.RegisterForm()
    if form.validate_on_submit():
        models.User.create_user(
            email=form.email.data,
            password=form.password.data
        )
        flash("You have successfully registered.", "success")
        return redirect(url_for('index'))
    return render_template('register.html', form=form)


### USER LOGIN
@app.route('/login', methods=('GET', 'POST'))
def login():
    form = forms.LoginForm()
    if form.validate_on_submit():
        try:
            user = models.User.get(models.User.email == form.email.data)
        except models.DoesNotExist:
            flash("Your email or password doesn't match!", "error")
        else:
            if check_password_hash(user.password, form.password.data):
                ### Create cookie for this session.
                login_user(user)
                flash("You have logged in successfully!", "success")
                return redirect(url_for('index'))
            else:
                flash("Your email or password doesn't match!", "error")
    return render_template('login.html', form=form)


### USER LOGOUT
@app.route('/logout')
@login_required
def logout():
    ### Delete cookie created earlier.
    logout_user()
    flash("Logout successfully! We might miss you...", "success")
    return redirect(url_for('index'))


### ADD A TACO
@app.route('/taco', methods=('GET', 'POST'))
@login_required
def taco():
    form = forms.TacoForm()
    if form.validate_on_submit():
        models.Taco.create(
            user=g.user._get_current_object(),
            protein=form.protein.data,
            shell=form.shell.data,
            cheese=form.cheese.data,
            extras=form.extras.data.strip()
        )
        flash("Taco added successfully! Thank you.", "success")
        return redirect(url_for('index'))
    return render_template('taco.html', form=form)


### HOMEPAGE
@app.route('/', methods=('GET', 'POST'))
def index():
    try:
        tacos = models.Taco.select().limit(100)
    except models.DoesNotExist:
        flash("We have no taco model..!", "error")

    return render_template('index.html', tacos=tacos)


### ERROR HANDLING
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


if __name__ == '__main__':
    models.initialize()
    try:
        models.User.create_user(
            email='test@testmail.com',
            password='secret123'
        )
    except ValueError:
        pass

    app.run(debug=DEBUG, host=HOST, port=PORT)
