import datetime

from flask_login import UserMixin
from flask_bcrypt import generate_password_hash, check_password_hash
from peewee import *

DATABASE = SqliteDatabase('tacocat.db')


### USER ENTITY
class User(UserMixin, Model):
    #username = CharField(unique=True)
    email = CharField(unique=True)
    password = CharField(max_length=100)
    joined_at = DateTimeField(default=datetime.datetime.now)
    #is_admin = BooleanField(default=False)

    class Meta:
        database = DATABASE
        ### This is tuple syntax, for ordering query
        order_by = ('-joined_at',)


    ### INSERT methods such as get_taco, following, followers, etc here
    def get_tacos(self):
        """Get all of current_user's tacos."""
        return Taco.select().where(Taco.user == self)


    @classmethod
    def create_user(cls, email, password):
        try:
            with DATABASE.transaction():
                cls.create(
                    email=email,
                    password=generate_password_hash(password)
                )
        except IntegrityError:
            raise ValueError("This username is already taken.")


### TACO ENTITY
# protein, a shell, a true/false for cheese, and a freeform area for extras
class Taco(Model):
    timestamp = DateTimeField(default=datetime.datetime.now)
    #user = ForeignKeyField(model=User, backref='tacos')
    user = ForeignKeyField(rel_model=User, related_name='tacos')

    protein = BooleanField()
    shell = BooleanField()
    cheese = BooleanField()
    extras = TextField()

    class Meta:
        database = DATABASE
        order_by = ('-timestamp',)


### INIT DB
def initialize():
    DATABASE.connect()
    ### Remember to initialize all the tables/models you need.
    DATABASE.create_tables([User, Taco], safe=True)
    DATABASE.close()
