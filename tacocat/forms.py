from flask_wtf import FlaskForm
from wtforms import (StringField, PasswordField, TextAreaField, SubmitField,
                        BooleanField)
from wtforms.validators import (DataRequired, Regexp, ValidationError, Email,
                                Length, EqualTo)

from models import User


def email_exists(form, field):
    if User.select().where(User.email == field.data).exists():
        raise ValidationError("This email address is not an available option.")


class RegisterForm(FlaskForm):
    email = StringField(
        'Email',
        validators=[
            DataRequired(),
            Email(),
            email_exists
            ])
    password = PasswordField(
        'Password',
        validators=[
            DataRequired(),
            Length(min=8),
            EqualTo('password2', message='Passwords must match.')
            ])
    password2 = PasswordField(
        'Confirm password',
        validators=[
            DataRequired()
            ])
    submit = SubmitField('Register')


class LoginForm(FlaskForm):
    email = StringField(
        'Email',
        validators=[
            DataRequired(),
            Email()
            ])
    password = PasswordField(
        'Password',
        validators=[
            DataRequired()
        ])
    submit = SubmitField('Log me in')


class TacoForm(FlaskForm):
    # TODO: radio for protein and shell, checkbox for cheese
    protein = BooleanField()
    shell = BooleanField()
    cheese = BooleanField()
    extras = TextAreaField("Anything else?")
