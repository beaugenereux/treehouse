const fs = require("fs");
const selenium = require('selenium-webdriver');
const By = selenium.By;

const HomePage = require('./pages/home.js');

const driver = new selenium.Builder().forBrowser('chrome').build();

// Moves content specific stuff of index.js to home.js
const homePage = new HomePage(driver);
homePage.open();
// So we can comment out the next line as the class HomePage will handle it.
//driver.get(process.env.URL);

const invitees = [
   'Gonzalo Torres del Fierro',
   'Shadd Anderson',
   'George Aparece',
   'Shadab Khan',
   'Joseph Michael Casey',
   'Jennifer Nordell',
   'Faisal Albinali',
   'Taron Foxworth',
   'David Riesz',
   'Maicej Torbus',
   'Martin Luckett',
   'Joel Bardsley',
   'Reuben Varzea',
   'Ken Alger',
   'Amrit Pandey',
   'Rafal Rudzinski',
   'Brian Lynch',
   'Lupe Camacho',
   'Luke Fiji',
   'Sean Christensen',
   'Philip Graf',
   'Mike Norman',
   'Michael Hulet',
   'Brent Suggs'
];
invitees.forEach(homePage.addInvitee, homePage);

// Remove Shadd for being a troll.
//homePage.removeInvitee("Shadd Anderson");

homePage
    .findInviteeByName("David Riesz")
    .remove();

homePage
    .findInviteeByName("Jennifer Nordell")
    .toggleConfirmation();

// fs stands for file system which allows us to do screenshots
// and save them locally.
// driver.takeScreenshot().then((image, err) => {
//     fs.writeFile("weird-layout.png", image, "base64",
//         err => console.error(err));
//     });

// Toggle the checkbox to hide confirmed invitees.
//homePage.toggleNonRespondersVisibility();

// *** TREEHOUSE CHALLENGE ***
// Create the sub-page code to rename an invitee.
homePage
    .findInviteeByName("Gonzalo Torres del Fierro")
    .changeName("Beau Genereux");
