from bs4 import BeautifulSoup
from selenium import webdriver

import time

driver = webdriver.Chrome()

driver.get('http://port-80-52540yz1ev.treehouse-app.com/')

time.sleep(5)

# Pulls the page source.
page_html = driver.page_source

# Parses the page source and intend it nicely.
soup = BeautifulSoup(page_html, 'html.parser')

print(soup.prettify())

by = selenium.By

field = driver.findElement(by.xpath("/html/body/div/header/form/input"))

field.sendKeys("Found this w/ explicit XPath")

# Stop the driver and close the automated website.
driver.close()
