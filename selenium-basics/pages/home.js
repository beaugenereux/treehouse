const selenium = require('selenium-webdriver');
const By = require('selenium-webdriver').By;

class HomePage {
    constructor(driver) {
        // Object initialization
        this.driver = driver;
        this.locators = {
            inviteeForm: By.id("registrar"),
            //inviteeNameField: By.name("name")
            // Replaced by a css selector for more precision.
            inviteeNameField: By.css("#registrar input[name='name']"),
            // *** Warning ***
            // while CSS selectors are powerful, they are also expensive.
            // Grab the first direct child <div> of <main> and
            // return the input fields of that child.
            toggleNonRespondersVisibility: By.css(".main  > div input"),
            // method inviteeByName requires a name, search for it and returns the parent.
            inviteeByName: name => By.xpath(`//span[text() = "${name}"]/..`),
        };
      }

    open() {
        // Normally we wouldn't use process.env.URL but this is a local project
        // therefore this process refers to our local environment (i think, BG)
        this.driver.get(process.env.URL);
        // If you are NOT launching this script by passing the URL,
        // comment above and uncomment below.
        // $ URL=http://port-80-52540yz1ev.treehouse-app.com/ node index.js
        //this.driver.get('http://port-80-52540yz1ev.treehouse-app.com/')
    }

    addInvitee(name) {
        this.driver.findElement(this.locators.inviteeNameField).sendKeys(name);
        this.driver.findElement(this.locators.inviteeForm).submit();
    }

    toggleNonRespondersVisibility() {
        this.driver.findElement(this.locators.toggleNonRespondersVisibility)
            .click();
    }

    findInviteeByName(name) {
        const el = this.driver
            .findElement(this.locators.inviteeByName(name));
            return new Invitee(el);
    }
}

// In JavaScript, this Invitee class is "private by default"
// because we are not exporting it as a module (see below).
class Invitee {
    constructor(element) {
        this.element = element;
        this.locators = {
            removeButton: By.css("button:last-child"),
            confirmedCheckbox: By.css("input[type='checkbox']"),
            // The commented lines belows are earlier classes (Page Object Model of Selenium Basics)
            // #invitedList button + button would bring the 2nd buttons found
            //#invitedList button:last-child
            // Another way of typing this would be:
            // By.xpath(`//span[text() = "${invitee}"]/parent::*/button[text()="remove"]`),
            //removeButtonForInvitee: invitee => By.xpath(`//span[text() = "${invitee}"]/../button[last()]`),
            editButton: By.css("button:first-of-type"),
            editNameField: By.css("input[type='text']"),
        };
    }

    remove() {
        this.element
            .findElement(this.locators.removeButton)
            .click();
    }

    toggleConfirmation() {
        this.element
            .findElement(this.locators.confirmedCheckbox)
            .click();
    }

    changeName(newname) {
        const button = this.element
            .findElement(this.locators.editButton);
        button.click();
        const textField = this.element
            .findElement(this.locators.editNameField);
        textField.clear();
        textField.sendKeys(newname);
        button.click();
    }
}

// If you import this file (home.js), you can export HomePage.
module.exports = HomePage;
