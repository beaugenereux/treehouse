# <html>
#   <body>
#
#     <form action="https://somesite.com/login" method="post" >
#       <input type="password" name="value" />
#       <button>Login!</button>
#     </form>
#
#     <form id="tweet-this" action="https://twitter.com/share" method="get">
#       <input type="textbox" name="value" />
#       <button>Send Tweet!</button>
#     </form>
#   </body>
# </html>

# Code to fill out the tweet form and submit it.
const form = driver.findElement(By.id("tweet-this"));
form.findElement(By.name("value")).sendKeys("Found it!");
form.submit();
