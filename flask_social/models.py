import datetime

from flask_login import UserMixin
from flask_bcrypt import generate_password_hash, check_password_hash
from peewee import *

DATABASE = SqliteDatabase('social.db')

# This is the user entity.
class User(UserMixin, Model):
    username = CharField(unique=True)
    email = CharField(unique=True)
    password = CharField(max_length=100)
    # We use .now instead of .now() to get datetime of Model
    # creation, not when this script is ran.
    joined_at = DateTimeField(default=datetime.datetime.now)
    # Later feature, just a placeholder here.
    is_admin = BooleanField(default=False)

    class Meta:
        database = DATABASE
        # Syntax explanation here:
        # https://teamtreehouse.com/library/build-a-social-network-with-flask/making-strong-users/the-user-model
        order_by = ('-joined_at',)

    def get_posts(self):
        """Get current_user's posts."""
        return Post.select().where(Post.user == self)

    def get_stream(self):
        """Get current_user's post and those from whom they follow."""
        ### Select all the posts (from those I follow or that are from me)
        return Post.select().where(
            (Post.user << self.following()) |
            (Post.user == self)
        )

    def following(self):
        """Get the users followed by current_user."""
        return (
            User.select().join(
                Relationship, on=Relationship.to_user
            ).where(
                Relationship.from_user == self
                )
            )

    def followers(self):
        """Get users following current_user."""
        return (
            User.select().join(
                Relationship, on=Relationship.from_user
            ).where(
                Relationship.to_user == self
            )
        )

    # cls as classmethod refers to a method required by the class itself
    # in order to operate an action, say create_and_send email.
    # https://teamtreehouse.com/library/build-a-social-network-with-flask/making-strong-users/class-method
    @classmethod
    def create_user(cls, username, email, password, admin=False):
        try:
            with DATABASE.transaction():
                cls.create(
                    username=username,
                    email=email,
                    password=generate_password_hash(password),
                    is_admin=admin)
        except IntegrityError:
            raise ValueError("This username is already taken.")


# This is the post entity.
class Post(Model):
    # Reminder: We use .now instead of .now() so we get
    # datetime of Model creation, not script running.
    timestamp = DateTimeField(default=datetime.datetime.now)
    # Course code might be outdated for end of 2020...
    #user = ForeignKeyField(rel_model=User, related_name='posts')
    user = ForeignKeyField(model=User, backref='posts')
    content = TextField()

    class Meta:
        database = DATABASE
        # Dealing with tuples means coma is needed, syntax rule.
        order_by = ('-timestamp',)


# This is the relationship entity.
class Relationship(Model):
    from_user = ForeignKeyField(model=User, backref='relationships')
    to_user = ForeignKeyField(model=User, backref='related_to')

    class Meta:
        database = DATABASE
        # This tuple links the required fields as unique index.
        indexes = (
            (('from_user', 'to_user'), True),
        )


def initialize():
    DATABASE.connect()
    # Remember to initialize all the tables you'll need.
    DATABASE.create_tables([User, Post, Relationship], safe=True)
    DATABASE.close()
