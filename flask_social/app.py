#!/Users/renard/code/treehouse/flask_social/venv/bin/python3
### The shabang line must be the first of the script.
### It ensures the app runs from its virtual environment,
### and not the local machine's root.

from flask import (Flask, g, render_template, flash, redirect, url_for,
                    abort)
from flask_bcrypt import check_password_hash
from flask_login import (LoginManager, login_user, logout_user,
                        login_required, current_user)

import forms
import models


# Placing this stuff here will speed up future setup say
# when moving to Heroku or another online service
DEBUG = True
PORT = 8080
HOST = '0.0.0.0'

app = Flask(__name__)
app.secret_key = '<skey>en#a&n;fé!4%kjlgo*--0000</skey>'

login_manager = LoginManager()
login_manager.init_app(app)
# If the user is not login, redirect to login view.
login_manager.login_view = 'login'


@login_manager.user_loader
def load_user(userid):
    try:
        return models.User.get(models.User.id == userid)
    except models.DoesNotExist:
        return None


@app.before_request
def before_request():
    """Connect to the database before each request."""
    g.db = models.DATABASE
    g.db.connect()
    # This allows to find the current user, flask_login methods.
    g.user = current_user


@app.after_request
def after_request(response):
    """Close the database connection after each request."""
    g.db.close()
    return response


### Routes for user registration live here. ###
@app.route('/register', methods=('GET', 'POST'))
def register():
    form = forms.RegisterForm()
    if form.validate_on_submit():
        flash("Registration successful!", "success")
        models.User.create_user(
            username=form.username.data,
            email=form.email.data,
            password=form.password.data
        )
        return redirect(url_for('index'))
    return render_template('register.html', form=form)


@app.route('/login', methods=('GET', 'POST'))
def login():
    form = forms.LoginForm()
    if form.validate_on_submit():
        try:
            user = models.User.get(models.User.email == form.email.data)
        except models.DoesNotExist:
            flash("Your email or password doesn't match!", "error")
        else:
            if check_password_hash(user.password, form.password.data):
                # Creates a cookie in the user's browser for the session.
                login_user(user)
                flash("You have logged in successfully!", "success")
                return redirect(url_for('index'))
            else:
                flash("Your email or password doesn't match!", "error")
    return render_template('login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    # Deletes the cookie to temrinate the session.
    logout_user()
    flash("You have logged out successfully.", "success")
    return redirect(url_for('index'))


@app.route('/new_post', methods=('GET', 'POST'))
@login_required
def post():
    form = forms.PostForm()
    if form.validate_on_submit():
        models.Post.create(user=g.user._get_current_object(),
                            content=form.content.data.strip())
        flash("Post successfully created.", "success")
        return redirect(url_for('index'))
    return render_template('post.html', form=form)


@app.route('/', methods=('GET', 'POST'))
@app.route('/index', methods=('GET', 'POST'))
def index():
    stream = models.Post.select().limit(100)
    # TODO:
    # Add pagination, check peewee docs
    # https://peewee.readthedocs.io/en/latest/peewee/playhouse.html?highlight=pagination#PaginatedQuery

    return render_template('stream.html', stream=stream)


@app.route('/stream')
@app.route('/stream/<username>')
def stream(username=None):
    template = 'stream.html'
    if username and username != current_user.username:
        try:
            ### The double asterix ** allows a case insensitive query
            user = models.User.select().where(
                models.User.username**username).get()
            ### If user not found, 404 please.
        except models.DoesNotExist:
            flash("Uh-oh. Interwebs, user not found.", "error")
            abort(404)
        else:
            stream = user.posts.limit(100)
    else:
        stream = current_user.get_stream().limit(100)
        user=current_user
    if username:
        template = 'user_stream.html'
    return render_template(template, stream=stream, user=user)


@app.route('/post/<int:post_id>')
def view_post(post_id):
    posts = models.Post.select().where(models.Post.id == post_id)
    ### If there is no posts, 404 please.
    # TODO: Call to action, or user recovery opportunity.
    if posts.count() == 0:
        flash("Interwebs, we have no single post!", "error")
        abort(404)
    return render_template('stream.html', stream=posts)


### What makes python or Flask understand < > as a variable? Why not the {{}} ?
@app.route('/follow/<username>')
@login_required
def follow(username):
    try:
        to_user = models.User.get(models.User.username**username)
    ### If user not found, 404 please.
    except models.DoesNotExist:
        flash("Uh-oh. Interwebs, user not found.", "error")
        abort(404)
    else:
        try:
            models.Relationship.create(
                from_user=g.user._get_current_object(),
                to_user=to_user
            )
        # TODO: Handling error if following relationship fails.
        except models.IntegrityError:
            pass
        else:
            flash("You are now following {}!".format(to_user.username), "success")
    return redirect(url_for('stream', username=to_user.username))


@app.route('/unfollow/<username>')
@login_required
def unfollow(username):
    try:
        to_user = models.User.get(models.User.username**username)
    ### If user not found, 404 please.
    except models.DoesNotExist:
        flash("Interwebs, impossible to unfollow user not found.", "error")
        abort(404)
    else:
        try:
            # Get the relationship and delete it (unfollow).
            models.Relationship.get(
                from_user=g.user._get_current_object(),
                to_user=to_user
            ).delete_instance()
        except models.IntegrityError:
            # TODO: Handling error if unfollowing fails.
            pass
        else:
            flash("You've unfollowed {}. Good riddance.".format(to_user.username), "success")
    return redirect(url_for('stream', username=to_user.username))


### 404 Not Found
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


### 401 Unauthorized
@app.errorhandler(401)
def unauthorized(error):
    return render_template('401.html'), 401


### Internal Server Error
@app.errorhandler(500)
def internal_server(error):
    return render_template('500.html'), 500


if __name__ == '__main__':
    models.initialize()
    try:
        models.User.create_user(
            username='Jack',
            email='test@testmail.com',
            password='secret123',
            admin=True
        )
    except ValueError:
        pass

    app.run(debug=DEBUG, host=HOST, port=PORT)
