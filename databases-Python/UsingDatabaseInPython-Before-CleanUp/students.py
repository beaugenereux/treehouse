from peewee import *

db = SqliteDatabase('students.db')


# The database model represent a singular element of a given model,
# therefore, the class Model is usually singular as well.
class Student(Model):
    username = CharField(max_length=255, unique=True)
    points = IntegerField(default=0)
    
    class Meta:
        database = db
        
        
# A dictionary to hold initial data.
students = [
    {'username': 'beaugenereux',
    'points': 3474},
    {'username': 'kennethlove',
    'points': 4888},
    {'username': 'joykesten2',
    'points': 11912},
    {'username': 'craigdennis',
    'points': 4079},
    {'username': 'davemcfarland',
    'points': 14717},
]

# A function to add the initial data to the database.
def add_students():
    for student in students:
        try:
            # For each entry in dict, create a db entry with its values.
            Student.create(username=student['username'],
                          points=student['points'])
        except IntegrityError:
                student_record = Student.get(username=student['username'])
                # TODO: Update points in the record if they changed.
                student_record.points = student['points']
                student_record.save()


# Fetch the top student.
def top_student():
    # Fetch all student entries, order them by points
    # in a descending order (biggest to smallest) and
    # get() the very first item. 
    student = Student.select().order_by(Student.points.desc()).get()
    return student

# If our file is read directly and not imported...
if __name__ == '__main__':
    db.connect()
    db.create_tables([Student], safe=True)
    add_students()
    print("Our top student right now is: {0.username}".format(top_student()))