from flask import jsonify, Blueprint

#from flask.ext.restful import Resource, reqparse, inputs
from flask_restful import (Resource, Api, reqparse,
                            inputs, fields, marshal,
                            marshal_with, url_for)

import models


ingredient_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'measurement_type': fields.String,
    'quantity': fields.Float,
    'recipe': fields.String
}


def add_ingredients(recipe):
    recipe.ingredients = [url_for('resources.recipes.recipe', id=recipe.id)
                        for recipe in course.ingredient_set]
    return recipe


def recipe_or_404(recipe_id):
    try:
        recipe = models.Recipe.get(models.Recipe.id==recipe_id)
    except models.Recipe.DoesNotExist:
        abort(404)
    else:
        return recipe


class IngredientList(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            'name',
            required=True,
            help='No name provided.',
            location=['form', 'json']
        )
        self.reqparse.add_argument(
            'description',
            required=True,
            help='No description provided.',
            location=['form', 'json']
        )
        self.reqparse.add_argument(
            'measurement_type',
            required=True,
            help='No measurement type provided.',
            location=['form', 'json']
        )
        self.reqparse.add_argument(
            'quantity',
            required=True,
            help='No quantity provided.',
            location=['form', 'json'],
            type=float
        )
        self.reqparse.add_argument(
            'recipe',
            required=True,
            help='No recipe provided.',
            location=['form', 'json'],
            type=inputs.positive
        )
        super().__init__()

    def get(self):
        ingredients = [marshal(ingredient, ingredient_fields)
                        for ingredient in models.Ingredient.select()]
        return {'ingredients': ingredients}
        #ingredients = models.Ingredient.select()
        #return ingredients

    @marshal_with(ingredient_fields)
    def post(self):
        args = self.reqparse.parse_args()
        ingredient = models.Ingredient.create(**args)
        return ingredient


class Ingredient(Resource):
    def __init__(self):
          self.reqparse = reqparse.RequestParser()
          self.reqparse.add_argument(
              'name',
              required=True,
              location=['form', 'json']
          )
          self.reqparse.add_argument(
              'description',
              required=True,
              location=['form', 'json']
          )
          self.reqparse.add_argument(
              'measurement_type',
              required=True,
              location=['form', 'json']
          )
          self.reqparse.add_argument(
              'quantity',
              type=float,
              required=True,
              location=['form', 'json']
          )
          self.reqparse.add_argument(
              'recipe',
              type=inputs.positive,
              required=True,
              location=['form', 'json']
          )
          super().__init__()

    @marshal_with(ingredient_fields)
    def get(self, id):
        ingredient = models.Ingredient.get(models.Ingredient.id==id)
        return ingredient


ingredients_api = Blueprint('resources.ingredients', __name__)
api = Api(ingredients_api)
api.add_resource(
    IngredientList,
    '/ingredients'
    endpoint='ingredients'
)
api.add_resource(
    Ingredient,
    '/ingredients/<int:id>',
    endpoint='ingredient'
)
