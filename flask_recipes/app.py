from flask import Flask

import models
from resources.recipes import recipes_api
from resources.ingredients import ingredients_api


app = Flask(__name__)
app.register_blueprint(recipes_api, url_prefix='/api/v1')
app.register_blueprint(ingredients_api, url_prefix='/api/v1')


if __name__ == '__main__':
    app.run()
