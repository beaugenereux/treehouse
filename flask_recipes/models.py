import datetime

from argon2 import PasswordHasher
from peewee import *

DATABASE = SqliteDatabase('recipes.db')
HASHER = PasswordHasher()


class User(Model):
    username = CharField(unique=True)
    password = CharField()

    class Meta:
        database = DATABASE

    @classmethod
    def create_user(cls, username, password):
        try:
            cls.get(cls.username**username)
        except cls.DoesNotExist:
            user = cls(username=username)
            user.password = user.hash_password(password)
            user.save()
            return user
        else:
            raise Exception("User already exists")

    @staticmethod
    def hash_password(password):
        return HASHER.hash(password)


class Recipe(Model):
    name = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = DATABASE


class Ingredient(Model):
    name = CharField()
    description = CharField()
    quantity = DecimalField()
    measurement_type = CharField()
    #recipe = ForeignKeyField(Recipe, related_name='ingredients' )
    recipe = ForeignKeyField(Recipe, backref='ingredients' )

    class meta:
        database = DATABASE
# TODO: Ingredient model
# name - string (e.g. "carrots")
# description - string (e.g. "chopped")
# quantity - decimal (e.g. ".25")
# measurement_type - string (e.g. "cups")
# recipe - foreign key
