#!/usr/bin/env python3

# Block of imports from Python
from collections import OrderedDict
import datetime
import os
import sys

# Block of imports from 3rd parties
from peewee import *
from textwrap3 import wrap
from blessings import Terminal


db = SqliteDatabase('diary.db')
t = Terminal()


class Entry(Model):
    # Textfield() doesn't care how many characters are entered (no limit?)
    content = TextField()
    # .now is used instead of .now() so the date is from the
    # entry creation and not script running.
    timestamp = DateTimeField(default=datetime.datetime.now)


    class Meta:
        database = db



def initialize():
    """Create the database and the table if they don't exist."""
    db.connect()
    db.create_tables([Entry], safe=True)


def clear():
    # Calls cls on Windows and nt on Mac, Linux, etc.
    os.system('cls' if os.name == 'nt' else 'clear')


def menu_loop():
    """Show the menu."""
    # Setting a variable to None is like $ var x; in JS
    choice = None

    while choice != 'q':
        # Clear the console when displaying menu loop
        clear()
        print("Enter 'q' to quit.")
        for key, value in menu.items():
            # value.__doc__ returns the choice's Docstring
            print('{}) {}'.format(key, value.__doc__))
        choice = input('Action: ').lower().strip()

        if choice in menu:
            clear()
            # Call the choice's function in the menu
            menu[choice]()


def add_entry():
    """Add an entry."""
    print("Enter your entry. Press ctrl+d when you are done.")
    # stdin refers to STandarD INput stream, usually the keyboard...
    data = sys.stdin.read().strip()

    if data:
        if input('Save entry? [Yn] ').lower() != 'n':
            Entry.create(content=data)
            print("Saved successfully!\n")


# use textwrap
# use Blessings
# Make application more professional and interesting




def view_entries(search_query=None):
    """View previous entries."""
    entries = Entry.select().order_by(Entry.timestamp.desc())
    if search_query:
        entries = entries.where(Entry.content.contains(search_query))

    for entry in entries:
        timestamp = entry.timestamp.strftime('%A %B %d, %Y %I:%M%p')
        clear()
        print(timestamp)
        print('='*len(timestamp))
        print(entry.content)
        print('\n\n'+t.bold('='*len(timestamp)))
        print('\n')
        print('n) Next entry')
        print('d) Delete entry')
        print('q) Return to main menu')

        next_action = input('Action: [Ndq] ').lower().strip()
        if next_action == 'q':
            break
        elif next_action == 'd':
            delete_entry(entry)


def search_entries():
    """Search entries for a string."""
    view_entries(input('Search query: '))



def delete_entry(entry):
    """Delete an entry."""
    if input("Are you sure? [yN] ").lower() == 'y':
        entry.delete_instance()
        print("Entry deleted!\n")


# Using an ordered dictionary as a menu simulates a Switch Case statement.
# The key is then linked its corresponding function.
menu = OrderedDict([
        ('a', add_entry),
        ('v', view_entries),
        ('s', search_entries),
])

if __name__ == '__main__':
    initialize()
    menu_loop()
