# https://github.com/lepture/flask-wtf/issues/304
#from flask_wtf import Form # Will soon be deprecated
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, TextAreaField, SubmitField
from wtforms.validators import (DataRequired, Regexp, ValidationError, Email,
                                Length, EqualTo)

from models import User


def name_exists(form, field):
    # I don't understand why we need this here since we already
    # have a ValueError for that during create_user method in models.py
    if User.select().where(User.username == field.data).exists():
        raise ValidationError("This username is already taken.")


def email_exists(form, field):
    if User.select().where(User.email == field.data).exists():
        raise ValidationError("This email address belongs to an existing account.")


class RegisterForm(FlaskForm):
    username = StringField(
    'Username',
    validators=[
        DataRequired(),
        Regexp(
        r'^[a-zA-Z0-9_]+$',
        message=("Username must be one word. "
        "Only letters, numbers and underscores are allowed.")
        ),
        name_exists
    ])
    email = StringField(
        'Email',
        validators=[
            DataRequired(),
            Email(),
            email_exists
    ])
    password = PasswordField(
        'Password',
        validators=[
            DataRequired(),
            Length(min=8),
            EqualTo('password2', message='Passwords must match.')
        ])
    password2 = PasswordField(
        'Confirm Password',
        validators=[DataRequired()]
    )
    # https://hackersandslackers.com/flask-wtforms-forms/
    submit = SubmitField('Submit')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])


class PostForm(FlaskForm):
    content = TextAreaField("How are you feeling right now?", validators=[DataRequired()])
