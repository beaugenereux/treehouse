import datetime

from flask.ext.bcrypt import generate_password_hash
from flask.ext.login import UserMixin
from peewee import *

DATABASE = SqliteDatabase(':memory:')


class User(UserMixin, Model):
    email = CharField(unique=True)
    password = CharField(max_length=100)
    join_date = DateTimeField(default=datetime.datetime.now)
    bio = CharField(default='')

    class Meta:
        database = DATABASE

    @property
    def following(self):
        return (
            User
            .select()
            .join(Relationship, on=Relationship.to_user)
            .where(Relationship.from_user == self)
        )

    @property
    def followers(self):
        return (
            User
            .select()
            .join(Relationship, on=Relationship.from_user)
            .where(Relationship.to_user == self)
        )


    @classmethod
    def new(cls, email, password):
        cls.create(
            email=email,
            password=generate_password_hash(password)
        )


class LunchOrder(Model):
    order = TextField()
    date = DateField()
    user = ForeignKeyField(User, related_name="orders")


class Relationship(Model):
    from_user = ForeignKeyField(User, related_name="relationships")
    to_user = ForeignKeyField(User, related_name="related_to")
    created_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = DATABASE
        indexes = (
            (('from_user', 'to_user'), True),
        )


def initialize():
    DATABASE.connect()
    DATABASE.create_tables([User, LunchOrder], safe=True)
    DATABASE.close()
