from flask import Flask
from flask import render_template
#from flask import request

# Create the app
app = Flask(__name__)


# Define your routes and create 'index.html' view
@app.route('/')
@app.route('/<name>')
# If no value was passed in url, default to "Beau"
def index(name="Beau"):
    #name = request.args.get('name', name)
    #return f"{name} is learning Python on Treehouse."
    return render_template('index.html', name=name)

@app.route('/add/<int:num1>/<int:num2>')
@app.route('/add/<float:num1>/<float:num2>')
@app.route('/add/<int:num1>/<float:num2>')
@app.route('/add/<float:num1>/<int:num2>')
def add(num1, num2):
    #return f"{num1} + {num2} = {num1+num2}"
    context = {'num1': num1, 'num2': num2}
    return render_template('add.html', **context)

# @app.route('/404')
# def 404():
#     return "Sorry, that didn't work."

@app.route('/')
@app.route('/multiply')
@app.route('/multiply/<int:num1>/<int:num2>')
@app.route('/multiply/<float:num1>/<float:num2>')
@app.route('/multiply/<int:num1>/<float:num2>')
@app.route('/multiply/<float:num1>/<int:num2>')
def multiply(num1=5, num2=5):
    return f"{num1*num2}"

# Define mode, port and host for the development app.
# Note: debug mode detects
app.run(debug=True, port=8000, host='0.0.0.0')
