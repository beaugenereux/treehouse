import json

from flask import (Flask, render_template, redirect,
                    url_for, request, make_response,
                    flash)

from options import DEFAULTS

app = Flask(__name__)
app.secret_key = '<skey>da*Sjdk,é2387y@*&a--0000</skey>'

# Returns the character cookies.
def get_saved_data():
    try:
        # loadstring() returns data as Python dictionary.
        data = json.loads(request.cookies.get('character'))
    except TypeError:
        # if it doesn't work, returns empty dictionary.
        data = {}
    return data


@app.route('/')
def index():
    data = get_saved_data()
    return render_template('index.html', saves=data)


@app.route('/builder')
def builder():
    return render_template('builder.html', saves=get_saved_data(),
                            options=DEFAULTS)


# Method for POST only
@app.route('/save', methods=['POST'])
def save():
    flash("Saved your bear changes.")
    # pdb will be great for optimization and debugging
    # if I understood correctly, for now, let's comment out.
    # import pdb; pdb.set_trace()

    # Get the name set in the index page form,
    # then redirect user to desired route (url_for).
    response = make_response(redirect(url_for('builder')))

    data = get_saved_data()
    data.update(dict(request.form.items()))
    # Extract name string from tuple dict with json,
    # then create a cookie for the character's name. (dumpstring())
    response.set_cookie('character', json.dumps(data))
    return response


# Launch app on local host server and port.
app.run(debug=True, host='0.0.0.0', port=8000)
